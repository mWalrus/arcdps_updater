#!/bin/bash
GRN='\033[1;32m'
NC='\033[0m'

INSTALL_PATH="$HOME/.local/share/Steam/steamapps/common/Guild Wars 2/bin64/d3d11.dll"

md5_filename="d3d11.dll.md5sum"
md5_old_filename="$md5_filename.old"

MD5URL="https://www.deltaconnected.com/arcdps/x64/d3d11.dll.md5sum"
DLLURL="https://www.deltaconnected.com/arcdps/x64/d3d11.dll"

# if an md5sum file exists in the working dir we mark it as old
if [ -f "$PWD/$md5_filename" ]; then
  mv $md5_filename $md5_old_filename
fi

# download the latest md5 sum from the website
printf "%sDownloading latest md5 sum...%s\n" "$GRN" "$NC"
curl $MD5URL -SLo "$PWD/$md5_filename" --progress-bar

printf "%sComparing the md5 sums...\n" "$GRN" "$NC"
# produces error if file is not found so we direct that to /dev/null cus we don't care
old_md5=$(cat "$PWD/$md5_old_filename" 2>/dev/null)
new_md5=$(cat "$PWD/$md5_filename")

if [ ! "$old_md5" = "$new_md5" ]; then
  printf "%sChecksum mismatch, downloading new ArcDPS version...%s\n" "$GRN" "$NC"
  curl $DLLURL -SLo "$INSTALL_PATH" --progress-bar
  echo "Downloaded file to $INSTALL_PATH"
  # it needs full rwx for all users
  printf "%sSetting correct file permissions...%s\n" "$GRN" "$NC"
  chmod +x "$INSTALL_PATH" && chmod 777 "$INSTALL_PATH"
  printf "%sDone!%s\n" "$GRN" "$NC"
else
  printf "%sNo Checksum mismatch, your ArcDPS is up-to-date! :^)%s\n" "$GRN" "$NC"
fi
